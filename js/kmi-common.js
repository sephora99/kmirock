// JavaScript Document

var imgRolloverSuffix = "_act";
var irsLen = imgRolloverSuffix.length;
var cache = new Array();

$(function () {
	$("#events-cal div:first").load("eventsCal.html");
	$("#featured-bands div:first").load("featuredBands.html");
	$("#now-playing div:first").load("nowPlaying.html");
	
	$("#generic-dialog").dialog({
		autoOpen: false,
		modal: true,
		buttons: {
			Ok: function () { $(this).dialog("close"); }
		}
	});
});

function openPlayer()
{
	$("#generic-dialog").html("The streaming media player is not yet active.").dialog("option", "title", "Coming Soon").dialog("open");
}

//IMAGE ROLLOVER HANDLERS
//These methods are passed into the jQuery .hover() method in order to handle rollovers
//They will replace the current image with one ending in the imgRolloverSuffix listed above
function hoverOver(evt) {
	var filename = $("img", evt.currentTarget).attr("src");		//Get the full image filename
	var fLen = filename.length;		//get the filename length
	//Test to see if the filename already ends in _act
	if (filename.substr(fLen-(irsLen+4), irsLen) != imgRolloverSuffix)
	{
		//Filename is not active, replace image
		filename = filename.substr(0, fLen-4) + imgRolloverSuffix + filename.substr(fLen-4);
		$("img", evt.currentTarget).attr("src", filename);
	}
}

function hoverOut(evt) {
	var filename = $("img", evt.currentTarget).attr("src");		//Get the full image filename
	var fLen = filename.length;		//get the filename length
	//Test to see if the filename already ends in _act
	if (filename.substr(fLen-(irsLen+4), irsLen) == imgRolloverSuffix)
	{
		//Filename is active, replace image
		filename = filename.substr(0, fLen-(irsLen+4)) + filename.substr(fLen-4);
		$("img", evt.currentTarget).attr("src", filename);
	}
}

//This will preload the rollover images
function preloadRollovers(elem){
	var cacheImage = document.createElement('img');
	var imgFilename = $("img", elem).attr('src');
	cacheImage.src = imgFilename.substr(0, imgFilename.length-4) + imgRolloverSuffix + imgFilename.substr(imgFilename.length-4);
	cache.push(cacheImage);
}

/*
//If there is text in this variable it will be displayed between the header and the main area on every page
var sitewideMessage = "Drs. Christine and Thomas Semenza invite you to an Open House on Wednesday March 24th 2010<br />(Snowdate Wednesday March 31st 2010). <a href='openHouse.html'>Click here for details.</a>";
//If there is no expiration date, set this equal to null
var sitewideMessageStart = new Date(2009,07,01,00,00,00,00);
var sitewideMessageEnd = new Date(2010,02,25,21,00,00,00);

function populateSitewideMessage()
{
	//If there is a message provided and today is not beyond the expiration date or there is no expiration date
	//Show the message in the message bar
	if(sitewideMessage && (!sitewideMessageEnd || new Date() < sitewideMessageEnd)
		&& (!sitewideMessageStart || new Date() > sitewideMessageStart))
	{
		var hm = document.getElementById('headerMessage');
		hm.innerHTML = "<span id='pulsingHeaderText' style='font-weight:bold;'>NOTICE:</span>&nbsp;" + sitewideMessage;
		hm.style.display = 'block';
		startPulsingText('pulsingHeaderText','FF0000','FFFFFF');
	}
}

function openSelfTest()
	{
		window.open ("selftest.html","selftest","menubar=0,resizable=0,scrollbars=1,width=500,height=700,top=30,left=30");
	}
	
	function submitOnEnter(event)
	{
		  if (event.keyCode == 13)  
          {  
              getDirections(); 
          }  
	}

function getDirections()
	{
		var fromAddr = document.getElementById("fromAddr").value;
		var toAddr = document.getElementById("toAddr").value;
		var mapPanel = document.getElementById("googleMap");
		var map = new GMap2(mapPanel);
		var directionsPanel = document.getElementById("googleDirections");
		var directions = new GDirections(map, directionsPanel);
		directions.load("from: " + fromAddr + " to: " + toAddr);
	}
	*/
